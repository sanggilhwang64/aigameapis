package shop.besthsg.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
