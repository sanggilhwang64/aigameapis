package shop.besthsg.api.game.logic.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.game.logic.enums.DfeState;
import shop.besthsg.api.game.logic.model.DfeConditionCreateRequest;
import shop.besthsg.api.game.logic.model.DfeConditionUpdateRequest;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DfeCondition {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "엔티티 이름")
    @Column(nullable = false, length = 50)
    private String dfeName;

    @ApiModelProperty(notes = "엔티티 상태")
    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private DfeState dfeState;

    @ApiModelProperty(notes = "수치 변화 여부")
    @Column(nullable = false)
    private Boolean isFigure;

    public void putDfeCondition(DfeConditionUpdateRequest updateRequest) {
        this.dfeName = updateRequest.getDfeName();
        this.dfeState = updateRequest.getState();
        this.isFigure = updateRequest.getIsFigure();
    }

    private DfeCondition(DfeBuilder builder) {
        this.dfeName = builder.dfeName;
        this.dfeState = builder.dfeState;
        this.isFigure = builder.isFigure;
    }

    public static class DfeBuilder implements CommonModelBuilder<DfeCondition> {
        private final String dfeName;
        private final DfeState dfeState;
        private final Boolean isFigure;

        public DfeBuilder (DfeConditionCreateRequest request) {
            this.dfeName = request.getDfeName();
            this.dfeState = request.getDfeState();
            this.isFigure = request.getIsFigure();

        }

        @Override
        public DfeCondition build() {
            return new DfeCondition(this);
        }
    }

}
