package shop.besthsg.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.besthsg.api.game.logic.entity.DfeCondition;
import shop.besthsg.api.game.logic.model.DialogFlowResponse;
import shop.besthsg.api.game.logic.model.GameRequest;
import shop.besthsg.api.game.logic.model.GameResponse;
import shop.besthsg.api.game.logic.repository.DfeConditionRepository;
import shop.besthsg.api.game.logic.repository.DieConditionRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GameService {
    private final DieConditionRepository dieConditionRepository;
    private final DfeConditionRepository dfeConditionRepository;
    private final DialogFlowService dialogFlowService;

    public GameResponse doCalculation(GameRequest gameRequest) {
        DialogFlowResponse dialogFlowResponse = dialogFlowService.detectIntentTexts(gameRequest.getMessage());

        // ------즉사여부 확인 시작-------
        // DieCondition에서 위에서 가져온 displayName에 해당하는 값이 있는지 확인한다. --> count로 확인
        long intentDuplicateCount = dieConditionRepository.countByIntentName(dialogFlowResponse.getIntentName());

        // 만약 데이터가 있으면.. 즉사처리하고
        if (intentDuplicateCount > 0) {
            return new GameResponse.GameResponseBuilder(dialogFlowResponse.getQueryText(), dialogFlowResponse.getFulfillmentText(), true, 0).build();
        } else { // 데이터가 없으면 점수 계산하는 2단계로 넘어간다.
            return new GameResponse.GameResponseBuilder(dialogFlowResponse.getQueryText(), dialogFlowResponse.getFulfillmentText(), false, calculateChangeVitalityFigures(dialogFlowResponse.getEntitys())).build();
        }
        // -----즉사여부 확인 끝 -----
    }

    private int calculateChangeVitalityFigures(List<String> paramKeys) {
        List<DfeCondition> dfeConditions = new LinkedList<>();

        // 내가 가진 키들의 상세 데이터를 dfeConditions에 모은다..
        for (String key : paramKeys) {
            Optional<DfeCondition> tempResult = dfeConditionRepository.findByDfeName(key);
            tempResult.ifPresent(dfeConditions::add);
        }

        int weightValue = 0; // 최종 가중치
        int weightCount = 0; // 수치 영향 여부 갯수
        for (DfeCondition dfeCondition : dfeConditions) {
            weightValue += dfeCondition.getDfeState().getWeighted();
            if (dfeCondition.getIsFigure()) {
                weightCount += 1;
            }
        }

        return weightValue * weightCount;
    }
}
