package shop.besthsg.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.besthsg.api.game.logic.model.LoginRequest;
import shop.besthsg.api.game.logic.model.LoginResponse;
import shop.besthsg.api.game.logic.service.MemberService;
import shop.besthsg.common.response.model.SingleResult;
import shop.besthsg.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "회원관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "로그인")
    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberService.doLogin(loginRequest));
    }
}
