package shop.besthsg.api.game.logic.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import shop.besthsg.api.game.logic.entity.Member;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
}
