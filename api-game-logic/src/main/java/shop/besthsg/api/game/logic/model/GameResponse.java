package shop.besthsg.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameResponse {
    @ApiModelProperty(notes = "사람 입력값")
    private String humanQueryText;

    @ApiModelProperty(notes = "ai 응답값")
    private String aiFulfillmentText;

    @ApiModelProperty(notes = "사망 여부")
    private Boolean isDead;

    @ApiModelProperty(notes = "변화되는 생명력 수치")
    private Integer changeVitalityFigures;

    private GameResponse(GameResponseBuilder builder) {
        this.humanQueryText = builder.humanQueryText;
        this.aiFulfillmentText = builder.aiFulfillmentText;
        this.isDead = builder.isDead;
        this.changeVitalityFigures = builder.changeVitalityFigures;
    }

    public static class GameResponseBuilder implements CommonModelBuilder<GameResponse> {
        private final String humanQueryText;
        private final String aiFulfillmentText;
        private final Boolean isDead;
        private final Integer changeVitalityFigures;

        public GameResponseBuilder(String humanQueryText, String aiFulfillmentText, Boolean isDead, Integer changeVitalityFigures) {
            this.humanQueryText = humanQueryText;
            this.aiFulfillmentText = aiFulfillmentText;
            this.isDead = isDead;
            this.changeVitalityFigures = changeVitalityFigures;
        }

        @Override
        public GameResponse build() {
            return new GameResponse(this);
        }
    }
}
