package shop.besthsg.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.game.logic.entity.DfeCondition;
import shop.besthsg.api.game.logic.enums.DfeState;
import shop.besthsg.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DfeConditionItem {
    @ApiModelProperty(notes = "엔티티 이름")
    private String dfeName;

    @ApiModelProperty(notes = "엔티티 상태")
    private DfeState dfeState;

    @ApiModelProperty(notes = "수치 변화 여부")
    private Boolean isFigure;

    private DfeConditionItem(DfeConditionItemBuilder builder) {
        this.dfeName = builder.dfeName;
        this.dfeState = builder.dfeState;
        this.isFigure = builder.isFigure;
    }

    public static class DfeConditionItemBuilder implements CommonModelBuilder<DfeConditionItem> {
        private final String dfeName;
        private final DfeState dfeState;
        private final Boolean isFigure;

        public DfeConditionItemBuilder(DfeCondition dfeCondition) {
            this.dfeName = dfeCondition.getDfeName();
            this.dfeState = dfeCondition.getDfeState();
            this.isFigure = dfeCondition.getIsFigure();
        }

        @Override
        public DfeConditionItem build() {
            return new DfeConditionItem(this);
        }
    }
}
