package shop.besthsg.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.besthsg.api.game.logic.entity.DieCondition;
import shop.besthsg.api.game.logic.model.DieConditionCreateRequest;
import shop.besthsg.api.game.logic.model.DieConditionItem;
import shop.besthsg.api.game.logic.model.DieConditionUpdateRequest;
import shop.besthsg.api.game.logic.repository.DieConditionRepository;
import shop.besthsg.common.exception.CMissingDataException;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DieConditionService {
    private final DieConditionRepository dieConditionRepository;

    public void setDieCondition(DieConditionCreateRequest request) {
        DieCondition addData = new DieCondition.DieConditionBuilder(request).build();
        dieConditionRepository.save(addData);
    }

    public ListResult<DieConditionItem> getDieConditions() {
        List<DieCondition> originList = dieConditionRepository.findAll();

        List<DieConditionItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new DieConditionItem.DieConditionItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putDieCondition(long id, DieConditionUpdateRequest updateRequest) {
        DieCondition originData = dieConditionRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putDieCondition(updateRequest);

        dieConditionRepository.save(originData);
    }

    public void delDieCondition(long id) {
        dieConditionRepository.deleteById(id);
    }

}
