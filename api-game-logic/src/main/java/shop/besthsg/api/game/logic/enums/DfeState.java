package shop.besthsg.api.game.logic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DfeState {
    HIGHPOSITIVE("매우긍정", + 30),
    NOMALPOSITIVE("보통긍정", + 20),
    LOWPOSITIVE("조금긍정", + 10),
    DENIAL("부정", - 10),
    NEUTRALITY("중립", 0)
    ;

    private final String stateName;
    private final Integer weighted;
}
