package shop.besthsg.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.game.logic.entity.DieCondition;
import shop.besthsg.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DieConditionItem {
    @ApiModelProperty(notes = "인텐트 이름")
    private String intentName;

    private DieConditionItem(DieConditionItemBuilder builder) {
        this.intentName = builder.intentName;

    }

    public static class DieConditionItemBuilder implements CommonModelBuilder<DieConditionItem> {
        private final String intentName;

        public DieConditionItemBuilder(DieCondition dieCondition) {
            this.intentName = dieCondition.getIntentName();
        }

        @Override
        public DieConditionItem build() {
            return new DieConditionItem(this);
        }
    }
}
