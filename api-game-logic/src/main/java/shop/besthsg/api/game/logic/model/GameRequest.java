package shop.besthsg.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GameRequest {
    @ApiModelProperty(notes = "사람이 개복치에게 할 말")
    @NotNull
    @Length(min = 2, max = 50)
    private String message;
}
