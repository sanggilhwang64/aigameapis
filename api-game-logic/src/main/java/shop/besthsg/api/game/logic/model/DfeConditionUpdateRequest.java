package shop.besthsg.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.besthsg.api.game.logic.enums.DfeState;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DfeConditionUpdateRequest {
    @ApiModelProperty(notes = "엔티티 이름")
    @NotNull
    @Length(min = 2, max = 50)
    private String dfeName;

    @ApiModelProperty(notes = "엔티티 상태")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DfeState state;

    @ApiModelProperty(notes = "수치 변화 여부")
    @NotNull
    private Boolean isFigure;
}
