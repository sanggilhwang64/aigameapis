package shop.besthsg.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.besthsg.api.game.logic.model.DieConditionCreateRequest;
import shop.besthsg.api.game.logic.model.DieConditionItem;
import shop.besthsg.api.game.logic.model.DieConditionUpdateRequest;
import shop.besthsg.api.game.logic.service.DieConditionService;
import shop.besthsg.common.response.model.CommonResult;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "즉사 조건")
@RestController
@RequestMapping("/v1/die/condition")
@RequiredArgsConstructor
public class DieConditionController {
    private final DieConditionService dieConditionService;

    @ApiOperation(value = "즉사 조건 등록")
    @PostMapping("/new")
    public CommonResult setDieCondition(@RequestBody @Valid DieConditionCreateRequest request) {
        dieConditionService.setDieCondition(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "즉사 조건 목록")
    @GetMapping("/all")
    public ListResult<DieConditionItem> getDieCondition() {
        return ResponseService.getListResult(dieConditionService.getDieConditions(),true);
    }

    @ApiOperation(value = "즉사 조건 수정")
    @PutMapping("/condition/{id}")
    public CommonResult putDieCondition(@PathVariable long id, @RequestBody @Valid DieConditionUpdateRequest updateRequest) {
        dieConditionService.putDieCondition(id, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "즉사 조건 삭제")
    @DeleteMapping("/condition/del/{id}")
    public CommonResult delDieCondition(@PathVariable long id) {
        dieConditionService.delDieCondition(id);

        return ResponseService.getSuccessResult();
    }
}
