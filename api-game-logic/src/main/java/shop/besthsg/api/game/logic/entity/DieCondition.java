package shop.besthsg.api.game.logic.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.game.logic.model.DieConditionCreateRequest;
import shop.besthsg.api.game.logic.model.DieConditionUpdateRequest;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DieCondition {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "인텐트 이름")
    @Column(nullable = false, length = 100)
    private String intentName;

    public void putDieCondition(DieConditionUpdateRequest updateRequest) {
        this.intentName = updateRequest.getIntentName();
    }


    private DieCondition(DieConditionBuilder builder) {
        this.intentName = builder.intentName;

    }
    public static class DieConditionBuilder implements CommonModelBuilder<DieCondition> {
        private final String intentName;

        public DieConditionBuilder(DieConditionCreateRequest request) {
            this.intentName = request.getIntentName();
        }

        @Override
        public DieCondition build() {
            return new DieCondition(this);
        }
    }
}
