package shop.besthsg.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DialogFlowResponse {
    @ApiModelProperty(notes = "사람이 개복치에게 한 말")
    private String queryText;

    @ApiModelProperty(notes = "개복치가 사람에게  한 말")
    private String fulfillmentText;

    @ApiModelProperty(notes = "df - 의도 이름")
    private String intentName;

    @ApiModelProperty(notes = "df - 사용된 엔티티")
    private List<String> entitys;
}
