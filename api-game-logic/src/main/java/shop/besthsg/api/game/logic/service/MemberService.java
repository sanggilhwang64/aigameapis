package shop.besthsg.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.besthsg.api.game.logic.entity.Member;
import shop.besthsg.api.game.logic.model.LoginRequest;
import shop.besthsg.api.game.logic.model.LoginResponse;
import shop.besthsg.api.game.logic.repository.MemberRepository;
import shop.besthsg.common.exception.CMissingDataException;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public LoginResponse doLogin(LoginRequest loginRequest) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingDataException::new);

        if (!member.getPassword().equals(loginRequest.getPassword())) throw new CMissingDataException();

        return new LoginResponse.Builder(member).build();
    }
}
