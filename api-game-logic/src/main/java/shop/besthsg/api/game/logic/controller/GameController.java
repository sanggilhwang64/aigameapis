package shop.besthsg.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.besthsg.api.game.logic.model.GameRequest;
import shop.besthsg.api.game.logic.model.GameResponse;
import shop.besthsg.api.game.logic.service.GameService;
import shop.besthsg.common.response.model.SingleResult;
import shop.besthsg.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "게임")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/game")
public class GameController {
    private final GameService gameService;

    @ApiOperation(value = "개복치에게 말걸기")
    @PostMapping("/send")
    public SingleResult<GameResponse> sendMessage(@RequestBody @Valid GameRequest gameRequest) {
        return ResponseService.getSingleResult(gameService.doCalculation(gameRequest));
    }
}
