package shop.besthsg.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.besthsg.api.game.logic.model.DfeConditionCreateRequest;
import shop.besthsg.api.game.logic.model.DfeConditionItem;
import shop.besthsg.api.game.logic.model.DfeConditionUpdateRequest;
import shop.besthsg.api.game.logic.service.DfeConditionService;
import shop.besthsg.common.response.model.CommonResult;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "엔티티")
@RequiredArgsConstructor
@RequestMapping("/v1/dfe")
@RestController
public class DfeConditionController {
    private final DfeConditionService dfeConditionService;

    @ApiOperation(value = "엔티티 등록")
    @PostMapping("/new")
    public CommonResult setDfeCondition(@RequestBody @Valid DfeConditionCreateRequest request) {
        dfeConditionService.setDfeCondition(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "엔티티 목록")
    @GetMapping("/all")
    public ListResult<DfeConditionItem> getDfeCondition() {
        return ResponseService.getListResult(dfeConditionService.getDfeCondition(), true);
    }

    @ApiOperation(value = "엔티티 수정")
    @PutMapping("/update/{id}")
    public CommonResult putDfeCondition(@PathVariable long id, @RequestBody @Valid DfeConditionUpdateRequest updateRequest) {
        dfeConditionService.putDfeCondition(id, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "엔티티 삭제")
    @DeleteMapping("/del/{id}")
    public CommonResult delDfeCondition(@PathVariable long id) {
        dfeConditionService.delDfeCondition(id);

        return ResponseService.getSuccessResult();
    }
}
