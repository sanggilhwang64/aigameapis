package shop.besthsg.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.besthsg.api.game.logic.entity.DfeCondition;
import shop.besthsg.api.game.logic.model.DfeConditionCreateRequest;
import shop.besthsg.api.game.logic.model.DfeConditionItem;
import shop.besthsg.api.game.logic.model.DfeConditionUpdateRequest;
import shop.besthsg.api.game.logic.repository.DfeConditionRepository;
import shop.besthsg.common.exception.CMissingDataException;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DfeConditionService {
    private final DfeConditionRepository dfeConditionRepository;

    public void setDfeCondition(DfeConditionCreateRequest request) {
        DfeCondition addData = new DfeCondition.DfeBuilder(request).build();
        dfeConditionRepository.save(addData);
    }

    public ListResult<DfeConditionItem> getDfeCondition() {
        List<DfeCondition> originList = dfeConditionRepository.findAll();

        List<DfeConditionItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new DfeConditionItem.DfeConditionItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putDfeCondition(long id, DfeConditionUpdateRequest updateRequest) {
        DfeCondition originData = dfeConditionRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putDfeCondition(updateRequest);

        dfeConditionRepository.save(originData);
    }

    public void delDfeCondition(long id) {
        dfeConditionRepository.deleteById(id);
    }
}
