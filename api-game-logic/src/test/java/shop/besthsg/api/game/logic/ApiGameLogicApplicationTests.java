package shop.besthsg.api.game.logic;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import shop.besthsg.api.game.logic.model.DialogFlowResponse;
import shop.besthsg.api.game.logic.service.DialogFlowService;

import java.io.IOException;


@SpringBootTest
public class ApiGameLogicApplicationTests {
    @Autowired
    DialogFlowService dialogFlowService;

    @Test
    void contextLoads() throws IOException {
        String callMsg = "밥 먹을래?";
        DialogFlowResponse result = dialogFlowService.detectIntentTexts(callMsg);

        int a = 1;
    }
}
